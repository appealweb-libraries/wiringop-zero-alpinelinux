FROM arm64v8/alpine:3.13.1

RUN apk update && apk add --no-cache \
        gcc \
        binutils \
        build-base \
        libgcc \
        make \
        musl-dev \
        g++ \
        build-base \
        libc-dev \
        linux-headers \
        nano \
        git

WORKDIR /opt/gpio

RUN git clone https://gitlab.com/appealweb-libraries/wiringop-zero-alpinelinux.git

WORKDIR /opt/gpio/wiringop-zero-alpinelinux

RUN ./build

WORKDIR /opt/gpio

ENTRYPOINT ["sh"]
